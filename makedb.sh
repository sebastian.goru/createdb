#!bin/sh
FUNC_FILES_SETTINGS="mnt/settings/functions/f_*.sql"
PROC_FILES_SETTINGS="mnt/settings/procedures/p_*.sql"
FUNC_FILES_BILLING="mnt/billing/functions/f_*.sql"
PROC_FILES_BILLING="mnt/billing/procedures/p_*.sql"
psql -U $POSTGRES_USER -d $POSTGRES_DB -f /mnt/settings/up-tables-settings-silin-db.sql
psql -U $POSTGRES_USER -d $POSTGRES_DB -f /mnt/settings/insert-settings-silin-db.sql
psql -U $POSTGRES_USER -d $POSTGRES_DB -f /mnt/billing/up-tables-billing-silin-db.sql
psql -U $POSTGRES_USER -d $POSTGRES_DB -f /mnt/billing/inserts-billing-silin-db.sql
for f in $FUNC_FILES_SETTINGS
do
  psql -U $POSTGRES_USER -d $POSTGRES_DB -f $f
done
for f in $PROC_FILES_SETTINGS
do
  psql -U $POSTGRES_USER -d $POSTGRES_DB -f $f
done
for f in $FUNC_FILES_BILLING
do
  psql -U $POSTGRES_USER -d $POSTGRES_DB -f $f
done
for f in $PROC_FILES_BILLING
do
  psql -U $POSTGRES_USER -d $POSTGRES_DB -f $f
done
