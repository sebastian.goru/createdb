# For (re) creating silindb

Note: Down functions are not required, therefore missing

## Instructions of use

### Initialization

1. Init volume and network, make sure your other service
  network is configured. By executing:
     - ./init.sh

2. docker-compose up

### Make the database

1. Container must be running, and execute:
  - ./make.sh

### For remaking the database

1. docker-compose down
2. Re execute the initialization command:
  - ./init.sh
3. docker-compose up
4. Make the database again:
  - ./make.sh

